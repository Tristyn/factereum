// AmdLoader required to make amd compatible with node
import { Server } from "./Server";
import * as Http from "http";
import { SIGINT } from "constants";

var server = Server.bootstrap();

//get port from environment and store in Express.
var port = normalizePort(process.env.PORT || 8080);
server.app.set("port", port);

//create http server
var http = Http.createServer(server.app);

//add error handler
http.on("error", onError);

//listen on provided ports
http.listen(port);

// handle input from stdin
// Windows doesnt like sending ^C through std in :( so terminate on any input
// process.stdin.resume();
process.stdout.write(`Listening on port ${port}; Press enter to terminate.\n`);
// process.stdin.on("data", function(chunk: any) { // called on each line of input
  // //var line : string = chunk.toString().replace(/\n/,"\\n");
  
  // //if (line === "exit\\n") {
  // http.close();
  // process.exit();
  // //} 
// });

/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(val: any) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error: any) {
  if (error.syscall !== "listen") {
    throw error;
  }

  var bind = typeof port === "string"
    ? "Pipe " + port
    : "Port " + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      console.error(bind + " requires elevated privileges");
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(bind + " is already in use");
      process.exit(1);
      break;
    default:
      throw error;
  }
}
