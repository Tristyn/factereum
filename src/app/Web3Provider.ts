var Web3 = require("web3");

declare var web3: any;
declare var window: any;

export interface IWeb3Provider {
	Get(): any;
}

export class MainWeb3Provider implements IWeb3Provider { 

	web3: any;

	public Get(): any {
		
	  	// Checking if Web3 has been injected by the browser (Mist/MetaMask)
		if ("web3" in this) {
			return this.web3;
		} 
	  	else if ("web3" in window) {
			// Use Mist/MetaMask's provider
			return this.web3 = new Web3(window.web3.currentProvider);
	  	} else {
			throw new Error("no metamask");
	  	}	    
	}
}


// Ganache is not supported in the browser
// export class LocalWeb3Provider implements IWeb3Provider {
// 	web3: Web3 | "undefined";

// 	public Get(): Web3 {
// 		if (this.web3 !== "undefined") {
// 			return this.web3;
// 		}

// 		this.web3 = new Web3(Ganache.provider());
// 		return this.web3;
// 	}
// } 