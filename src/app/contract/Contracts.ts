import * as assert from "assert"; 
// A collection of truffle contracts for the app.

export class Contracts {
	factereum: any;
	store: any;

	constructor (factereum: any, store: any) {
		assert(factereum != null && store != null);

		this.factereum = factereum;
		this.store = store;
	}
}