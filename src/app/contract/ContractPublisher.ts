var Web3 = require("web3");
import { IProvider as IWeb3Provider } from "web3/types";
import { Contracts } from "./Contracts";
var Contract = require("truffle-contract");

declare type ContractDefs = {
	Factereum: any;
	Store: any;
};

export class ContractPublisher {
	contractDefs: ContractDefs;
	ceoAddress: string;
	web3: any;

	// cfg
	_pause: boolean;
	_upgradingFrom: string;


	/**
	 * Contract Deployer the app contracts, links and initializes them.
	 * Will deploy even the contract already exists on the network.
	 * The contracts will be initialized with no default transaction values.
	 * @param web3 The network to deploy to.
	 * @param ceoAddress The address that will publish and own the main contract.
	 */
	constructor(web3: any, ceoAddress: string, contractDefs: any) {
		this.web3 = web3;
		this.ceoAddress = ceoAddress;
		this.contractDefs = contractDefs;

		// Start paused, just like the contract implementation.
		this.pause = true;
	}
	
	/**
	 * Gets if the contract will be initialized into the `paused` state.
	 */
	public get pause() : boolean {
		return this._pause;
	}

	/**
	 * Sets if the contract will be initialized into the `paused` state.
	 */
	public set pause(v : boolean) {
		this._pause = v;
	}
	
	
	public get upgradingFrom() : string {
		return this._upgradingFrom;
	}

	/**
	 * Sets the Factereum V1 contract that will be upgraded, which denotes that 
	 * the new Factereum contract is V2.
	 * Null denotes that the newly published Factereum contract is V1.
	 */
	public set upgradingFrom(factereumV1Address: string) {
		this._upgradingFrom = factereumV1Address;
	}
	
	

	/**
	 * Publishes the contracts, or throws an error.
	 */
	public async publish() {
		var transactionParams = { 
			from: this.ceoAddress
		};

		return this._publish(transactionParams);
	}

	private async _publish(transactionParams: any) {
		

		// deploy store
		var store = Contract(this.contractDefs.Store);
		store.setProvider(this.web3.currentProvider);
		store = await store.new(transactionParams);

		// deploy factereum
		var factereum = Contract(this.contractDefs.Factereum);
		factereum.setProvider(this.web3.currentProvider);
		factereum = await factereum.new(transactionParams);
		
		// link contracts and upgrade
		await Promise.all([
			factereum.setStoreAddress(store.address),
			store.setWriter(factereum.address),
			this.upgradingFrom != null ? 
				this._upgradeFactereum(factereum.address) : null,
		]);

		if (this.pause === false) {
			await factereum.unpause();
		}

		return new Contracts(factereum, store);
	}

	private async _upgradeFactereum(v2Address: string) {
		if (this.upgradingFrom != null) {
			throw new Error();
		}

		var v1 = await Contract(this.contractDefs.Factereum).at(this.contractDefs.Factereum);
		await v1.setNewAddress(v2Address);
	}
}