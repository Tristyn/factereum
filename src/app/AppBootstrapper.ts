import { MainWeb3Provider } from "./Web3Provider";
import { SimpleEthNetworks, SimpleEthNetwork } from "./SimpleEthNetworks";
import { App } from "./App";
import { AppEnv, GameEnv } from "./AppEnv";
import { Repo } from "./Repo";
import { ContractPublisher } from "./contract/ContractPublisher";
import * as assert from "assert";
import * as contractDefs from "!json-loader!./../../bin/contracts/index.json";
var Web3 = require("web3");

export class AppBootstrapper {
	public async configure() : Promise<App> {
		var appEnv = new AppEnv();

		await this.ConfigWeb3(appEnv);

		await this.PublishContracts(appEnv.gameEnv);

		await this.ConfigRepo(appEnv.gameEnv);
		//await this.ConfigRepo(appEnv.localnet);

		return new App(appEnv);

	}

	async ConfigWeb3(appEnv: AppEnv) {
		var netEnv: GameEnv; // reused vars
		var netId: string;

		netEnv = appEnv.gameEnv = new GameEnv();
		netEnv.web3 = new MainWeb3Provider().Get();
		netId = <string> await new Promise(
			cb => (<any>netEnv.web3.eth).net.getId(cb));
		netEnv.isMainNet = netId === "1";
	}

	async PublishContracts(gameEnv: GameEnv) {
		var accounts = await gameEnv.web3.eth.getAccounts();
		var publisher = new ContractPublisher
			(gameEnv.web3, accounts[0], contractDefs);
		gameEnv.contracts = await publisher.publish();
	}

	ConfigRepo(gameEnv: GameEnv) {
		// use gameEnv.isMainNet to inject addresses when loading a local save 
		
		gameEnv.repo = new Repo(gameEnv.web3, gameEnv.contracts);
		return gameEnv.repo.Initialize();
	}
}