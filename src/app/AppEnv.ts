var Web3 = require("web3");
import { SimpleEthNetwork } from "./SimpleEthNetworks";
import { Repo } from "./Repo";
import { Contracts } from "./contract/Contracts";

export class AppEnv {
	gameEnv: GameEnv;
}

export class GameEnv {
	web3: any;
	isMainNet: boolean;
	repo: Repo;
	contracts: Contracts;
}