import * as Contracts from "!json-loader!./contracts.json";

export class SimpleEthNetworks {
	public static Get() {
		return <SimpleEthNetworks><any>Contracts;
	}

	development: SimpleEthNetwork;
	mainnet: SimpleEthNetwork;
}

export class SimpleEthNetwork {
	name: string;
	contracts: SimpleContract[];
}

export class SimpleContract {
	name: string;
	address: string;
	abi: any;
	
}