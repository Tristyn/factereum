import { AppBootstrapper } from "./AppBootstrapper";
var Web3 = require("web3");

declare var web3: any;
declare var document: any;
declare var window: any;

window.onload = async function() {
	if ("web3" in window === false) {

		var info = document.createElement("h2");
		info.textContent = "Install ";
		var link = document.createElement("a");
		link.textContent = "Metamask";
		link.setAttribute("href", "https://metamask.io/");
		info.appendChild(link);
		document.body.appendChild(info);	
		return;
	}

	await new Promise((resolve, reject) => { setTimeout(resolve, 5000); });
	// Get them to unlock metamask address so we can make transactions.
	var w3Tester = new Web3(window.web3.currentProvider);

	if ((await w3Tester.eth.getAccounts()).length === 0) {
		
		var unlockNotice = document.createElement("h2");
		unlockNotice.textContent = "Please unlock Metamask";
		document.body.appendChild(unlockNotice);	

		var numAccounts = 0;
		while (numAccounts === 0) {
			await new Promise(resolve => setTimeout(resolve, 500));
			w3Tester.currentProvider = window.web3.currentProvider;
			numAccounts = (await w3Tester.eth.getAccounts()).length;
		}

		unlockNotice.parentNode.removeChild(unlockNotice);
	}

	
	new AppBootstrapper().configure().then(app => app.Run());

	
};
