var Web3 = require("web3");
import { SimpleContract } from "./SimpleEthNetworks";
import * as Assert from "assert";
import { Contracts } from "./contract/Contracts";
var Contract = require("truffle-contract");

export class Repo {
	web3: any;
	contracts: Contracts;
	
	constructor(web3: any, contracts: Contracts) {
		
		this.web3 = web3;
		this.contracts = contracts;
	}

	// Sets contract provider and conditionally deploys to the network if the network
	// is 
	async Initialize() {
		
	}
}