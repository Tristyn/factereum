// Transform and consolidate chains.json and dist/contracts/Foo.json
// to a more usable format for the app to consume.
import * as Path from "path";
import * as Fs from "fs"; 

export class EmbarkChainsToContractInfoTransformer {
	// Refactor: seperate file access and transformations.
	// Provide chains object and contractDefintiions[] to this class,
	// load them in another class. Link both with third class.
	// The transform output is good, as it returns an object, not saves to file.

	contractDistDir: string;
	chainsFile: string;

	constructor(chainsFile: string,
		contractDistDir: string) {
		
		this.chainsFile = chainsFile;
		this.contractDistDir = contractDistDir;
	}

	public transform() {
		
		var err, data = Fs.readFileSync(this.chainsFile, "utf8");

    	if (err) {
	        throw err;
		}

   		var chains = JSON.parse(data);
		
		var result = { }


		for (var chainIndex in chains) {
			var chain = chains[chainIndex]
			var contracts = []
			for (var contractIndex in chain.contracts) {
				var contract = chain.contracts[contractIndex]
				contracts.push(this.transformContractDefinition(contract.name));
			};

			result[chain.name] = 
			{
				name: chain.name,
				contracts: contracts
			};

		};

		return result;
	}

	private transformContractDefinition(contractName: string) {
		
		var contractPath = Path.join(
			this.contractDistDir, contractName) + ".json";
		var err, data = Fs.readFileSync(contractPath, "utf8");

    	if (err) {
	        throw err;
		}

   		var contract = JSON.parse(data);
	
		var transformed = {
			name: contract.contract_name,
			address: contract.address,
			gasEstimates: contract.gasEstimates,
			abi: contract.abi
		};

		return transformed;
	}
}