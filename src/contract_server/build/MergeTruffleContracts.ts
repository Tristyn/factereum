// Merges all contract jsons to one file so it can be statically required and webpack
// can merge it into the bundle. Optionally strip info like source code and compiler
// version
import * as Path from "path";
import * as Fs from "fs"; 

export class MergeTruffleContracts {
	// Refactor: seperate file access and transformations.
	// Provide chains object and contractDefintiions[] to this class,
	// load them in another class. Link both with third class.
	// The transform output is good, as it returns an object, not saves to file.

	contracts: any[];
	isStripping: boolean;

	constructor(contracts: any[], stripSensitiveInfo: boolean) {
		
		this.contracts = contracts;
		this.isStripping = stripSensitiveInfo;
	}


	public merge() {
		var merged = { }

		this.contracts.forEach(contract => {
			var clone = JSON.parse(JSON.stringify(contract));
			
			if (this.isStripping === true){
				this.strip(clone);
			}

			merged[clone.contractName] = clone;
		});

		return merged;
	}


	strip(c: any) {
		c.sourceMap = undefined;
		c.deployedSourceMap = undefined;
		c.source = undefined;
		c.sourcePath = undefined;
		c.ast = undefined;
		c.compiler = undefined;
		c.updatedAt = undefined;

	}
}