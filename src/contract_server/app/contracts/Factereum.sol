pragma solidity ^0.4.17;

import "./Account.sol";

contract Factereum {

	// accounts
	//mapping (address => string) _addressToAccountName;
	//mapping (string => address) _accountNameToAddress;
	
	uint _numberOfAccounts;
	
	address _admin;

	// If a newer version of the factereum is available, force users to use it
	bool _modificationDisabled;

	function Factereum(address admin) {
		_admin = admin;
		_numberOfAccounts = 0;
		_modificationDisabled = false;
	}

	enum RegisterResult {
		Success,
		FailNameEmpty,
		FailNameTooLong,
		FailAlreadyRegistered,
		FailRegistrationDisabled
	}

	function Register(string name) returns (RegisterResult) {
		if (bytes(name).length == 0) {
			return RegisterResult.FailNameEmpty;
		}
		else if (bytes(name).length >= 64) {
			return RegisterResult.FailNameTooLong;
		}
		else if (bytes(_addressToAccountName[msg.sender]).length != 0) {
			return RegisterResult.FailAlreadyRegistered;
		}
		else if (_modificationDisabled){
			return RegisterResult.FailRegistrationDisabled;
		}
		else {
			_addressToAccountName[msg.sender] = name;
			_accountNameToAddress[name] = msg.sender;
			_numberOfAccounts++;
			return RegisterResult.Success;
		}
	}

	function suicide() returns (bool) {
		if (msg.sender == _admin)
		{			
			selfdestruct(_admin);
			return true;
		}
		return false;
	}
}