pragma solidity ^0.4.17;
//pragma experimental ABIEncoderV2;
// The current ABI encoder does not support passing structs from external calls

contract Surface {

	address _admin;

	struct Plot {
		int x;
		int y;
		int width;
		int height;
		bool for_sale;
		uint price;
		uint land_tax;
	}

	mapping (address => Plot[]) _addressToPlots;

	function Surface() {
		_admin = msg.sender;
	}

	function Mint(Plot plot) returns (bool) {
		if (msg.sender != _admin) {
			return false;
		}

		_addressToPlots[msg.sender].push(plot);
	}
	
	function Suicide() returns (bool) {
		if (msg.sender == _admin) {
			selfdestruct(_admin);
			return true;
		}
		return false;
	}
}