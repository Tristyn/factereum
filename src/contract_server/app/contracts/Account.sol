pragma solidity ^0.4.17;

contract Account {
	address _admin;

    function Account() {
		_admin = msg.sender;
	}

	function GetAdmin()
	returns (address) {

		return _admin;
	}
}