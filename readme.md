#Getting started

install vscode, node

install git bash, during install set:

* Use Npp as default commit editor
* use unix tools from windows command prompt (edits path)
* checkout windows-stye commit unix style

Install vscode extensions: Debugger for chrome, firefox, npm, solidity extended, tslint

install packages:
```bash
npm install
```

install truffle globally: (because it doesn't include args when run through npm run truffle)
```bash
npm install -g truffle
```


# Scripts folder

all commands must be run from the project root. "git bash here" is your best friend

```bash
./scripts/build.sh # Master build task, outputs to ./bin.
./scripts/run.sh # Runs the node server and starts a dev chain in the background. Can debug the browser in VSCode. Alternatively debug the server by launching through vscode
./scripts/runchain.sh # Runs a dev chain
./scripts/killnode.sh # Kills all node processes, they often leak when bash commands are cancelled

npm run webpack -- -w # Builds ./src/app and outputs to ./bin/static/app.js, -w watches and recompiles changes.
npm run grunt # Builds ./src/public_server and outputs to ./bin. Runs misc build tasks.
```
