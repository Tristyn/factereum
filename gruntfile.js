const fs = require("fs");
const path = require("path");
const webpackConfig = require('./webpack.config');

module.exports = function(grunt) {
	"use strict";
  
	grunt.initConfig({
	  	ts: {
			server: {
		  		files: [{
					src: ["src/public_server/*.ts", "!src/public_server/app/*.ts"],
			  		dest: "bin"
				}],
				//tsconfig: "tsconfig.json",
		  		options: {
					module: "commonjs",
					moduleResolution: "node",
					noLib: false,
					target: "es2017",
					sourceMap: true,
					rootDir: "src/public_server",
				},
			},
			contract_server: {
				files: [{
					// Using generic ./src/contract_server/**/*.ts, dest: "." results
					// in .js being put in ./build ???? so compile the file directly.
					src: ["./src/contract_server/**/*.ts" ],
					dest: "."
				}],
				//tsconfig: "tsconfig.json",
				options: {
					module: "commonjs",
					moduleResolution: "node",
					noLib: false,
					target: "es2017",
					sourceMap: true,
					rootDir: ".",
				}
			},

	  	},
	  	tslint: {
			options: {
			  configuration: "tslint.json"
			},
			files: {
			  src: ["src/\*\*/\*.ts"]
			}
		},

		clean: {
			content: ["bin/static/*", "!bin/static/app.{js,js.map}"],
			views: "bin/views/*",
			typescriptTemp: "tscommand-*.tmp.txt"
		},

		copy: {
	  		content: {
	  			files: [{
						expand: true,
						cwd: "src/public_server/content/",
						src: "*", 
						dest: "bin/static/"
					}]
			},
			views: {
				files: [{
					expand: true,
					cwd: "src/public_server/views/",
					src: "*", 
					dest: "bin/views/"
				}]
			}
		},
		
		webpack: {
      		options: {
        		//stats: false
      		},
      		prod: webpackConfig,
      		dev: webpackConfig
    	},

	  	watch: {
			options: {
				interrupt: true,
				livereload: true
			},
			server: {
				files: ["src/public_server/**/*.ts"],
				tasks: ["clean", "copy",	"ts"]
			},
			copy: {
		  		files: ["src/public_server/views/**/*", "src/public_server/content/**/*"],
		  		tasks: ["clean", "copy"]
		  	},
		}	

	});

	grunt.registerTask("default", [
		"clean",
		"copy",
		"ts",
		"mergeContracts"
	]);


	
	grunt.loadNpmTasks("grunt-contrib-watch");
	grunt.loadNpmTasks("grunt-ts");
	grunt.loadNpmTasks("grunt-tslint");
	grunt.loadNpmTasks("grunt-contrib-copy")
	grunt.loadNpmTasks("grunt-contrib-clean")
	grunt.loadNpmTasks("grunt-webpack")

	grunt.registerTask("embarkContractsJson", function (chainsFile, contractDistDir, outDir) {
		
		// Require this in-method because it gets compiled in a previous task
		const EmbarkChainsToContractInfoTransformer 
			= require("./src/contract_server/build/" +
			"EmbarkChainsToContractInfoTransformer");

		var transformer = new EmbarkChainsToContractInfoTransformer
			.EmbarkChainsToContractInfoTransformer(
			chainsFile, contractDistDir);
		var result = transformer.transform();
		var data = JSON.stringify(result, null, 2);
		// Remove the byte order mask, it fucks with webpack
		data = data.replace(/^\uFEFF/, '');
		fs.writeFileSync(outDir, data)
	});

	grunt.registerTask("mergeContracts", function() {
		var contractsDir = path.resolve("./bin/contracts");
		var out = path.resolve("./bin/contracts/index.json");
		
		var contracts = fs.readdirSync(contractsDir)
			.map(name => path.join(contractsDir, name))
			.filter(path => path !== out)
			.map(path => fs.readFileSync(path))
			.map(json => JSON.parse(json));

		var Merger = require("./src/contract_server/build/MergeTruffleContracts")
			.MergeTruffleContracts;
		var merger = new Merger(contracts, true);
		var merged = merger.merge();
		fs.writeFileSync(out, JSON.stringify(merged, null, 2));	
	});

	grunt.registerTask("truffleArtifactor", function(truffleContractsDir, outDir) {
		// THIS DOESNT WORK, artifactor.save doesnt output anything
		
		// TruffleArtifactor README.md is out of date, use:
		// https://github.com/trufflesuite/truffle-artifactor/issues/49#issuecomment-327528651
		truffleContractsDir = path.resolve(truffleContractsDir);
		var taskCompleteCb = this.async();
		var Artifactor = require("truffle-artifactor");
		var artifactor = new Artifactor(path.resolve("./bin/contracts_mainnet/"))
		

		fs.readdir(truffleContractsDir, (err, files) => {
			if (err) throw err;

			files.forEach(file => {
				// output of "truffle compile"
				var truffleContract = require(path.join(truffleContractsDir,file));
				var out = "test.sol.js";
				var deployedAddress = "undefined";
				var mainnetContract = truffleContract.networks["1"];
				if (mainnetContract !== undefined) {
					deployedAddress = mainnetContract.address;
				}
				var options = {
					contract_name: truffleContract.contractName,
					abi: truffleContract.abi,
					unlinked_binary: truffleContract.binary,
					default_network: 1,
					address: deployedAddress
				}

				artifactor.save(options, "foo.js");
			});

			taskCompleteCb()
		})
		
		

	});


	/*
	grunt.registerTask("embark_build", function (embark_dir, embark_env) {
		var project_dir = process.cwd()
		spawn("node_modules/.bin/embark",["build", embark_env], {cwd = embark_dir, shell = true})
		chdir(embark_dir, function() {
			if (true) {
				var cmd = 'npm.cmd'
			} else {
				var cmd = 'npm'
			}
			
			spawn(resolvePath(project_dir, "node_modules/.bin/embark.cmd"), ["build", embark_env], {cwd = embark_dir, shell = true})
			var embark = new (require("embark"))

			embark.initConfig(embark_env || 'development', {
				embarkConfig: 'embark.json',
				interceptLogs: false
			});
			embark.build({env: embark_env || 'development'});
		});
	});
  
	grunt.registerTask("embark_simulator", function (embark_dir, embark_env) {
		
		chdir(embark_dir, function() {
			var embark = new (require("embark"))
			embark.initConfig(embark_env || 'development', {
				embarkConfig: 'embark.json',
				interceptLogs: false
			});
			embark.simulator({port: 8000, host: "localhost"});


		})		
	});
*/



};
	

