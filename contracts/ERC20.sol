pragma solidity ^0.4.18;

// https://github.com/ethereum/EIPs/issues/20

contract ERC20 {
	// Required methods
    function totalSupply() public constant returns (uint total);
    function balanceOf(address _owner) public constant returns (uint balance);
    function transfer(address _to, uint _value) public returns (bool success);
    function transferFrom(address _from, address _to, uint _value) public returns (bool success);
    function approve(address _spender, uint _value) public returns (bool success);
    function allowance(address _owner, address _spender) public constant returns (uint remaining);

	// Events
    event Transfer(address indexed _from, address indexed _to, uint _value);
    event Approval(address indexed _owner, address indexed _spender, uint _value);

	// Optional
	// string public constant name = "Token Name";
	// string public constant symbol = "SYM";
	// uint8 public constant decimals = 18;  // 18 is the most common number of decimal places

    // ERC-165 Compatibility (https://github.com/ethereum/EIPs/issues/165)
    function supportsInterface(bytes4 _interfaceID) external view returns (bool);
}