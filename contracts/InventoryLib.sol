pragma solidity ^0.4.18;
pragma experimental ABIEncoderV2;

// Constructs for inventory management where aliases are used for item ids.
library InventoryLib {

	struct MemoryInventory {
		uint[] itemIdsMap;

		int[] shortInventory;

		StoreEntity.EntityInfo[] entities;
	}

	function getItemId(MemoryInventory self, uint itemIndex)
		internal pure returns(uint)
	{
		return self.itemIdsMap[itemIndex];
	}

	function getItemCount(MemoryInventory self, uint itemIndex)
		internal pure returns(uint)
	{
		var count = self.shortInventory[self.itemIdsMap[itemIndex]];
		if (count < 0) return 0;
		return count;
	}

	function incrementItemCount(MemoryInventory self, uint itemIndex)
		internal 
	{
		var count = self.shortInventory[self.itemIdsMap[itemIndex]];
		if (count < 0) count = 0;
		self.shortInventory[self.itemIdsMap[itemIndex]] = count + 1;
	}

	function decrementItemCount(MemoryInventory self, uint itemIndex)
		internal 
	{
		var count = self.shortInventory[self.itemIdsMap[itemIndex]];
		require(count > -1);
		if (count == 1) count = 0;
		self.shortInventory[self.itemIdsMap[itemIndex]] = count - 1;
	}

	function getEntity(MemoryInventory self, uint itemIndex)
		internal pure
	{
		return self.entities[self.itemIdsMap[itemIndex]];
	}

}