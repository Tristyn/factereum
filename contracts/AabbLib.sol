pragma solidity ^0.4.18;

contract AabbLib {
	
	//struct Aabb(bytes32 container, uint8 index) {
	//	uint8 x1 = container[index];
	//	uint8 y1 = container[index + 1];
	//	uint8 x2 = container[index + 2];
	//	uint8 y2 = container[index + 3];
	//}

	
	// function collides(Aabb a, Aabb b) public constant returns (bool) {
	// 	return
	// 	a.x1 < b.x2 &
	// 	a.x2 > b.x1 &
	// 	a.y1 < b.y2 &
	// 	a.y2 > b.y1;
	// }

	// AABB collision detection
	function collides(bytes32 aData, bytes32 bData, uint8 aIndex, uint8 bIndex)
		internal pure returns (bool)
	{
		// cheap expressions before short-circuit && ops are ordered first.
		// Only pay for a couple jumpi ops (&&)
		// The huristic is if the first AND is correct more likely a collision
		
		return (aData[aIndex] < bData[bIndex + 2])	   // 12g
			 && (aData[aIndex + 2] > bData[bIndex])	   // 12g
			 && (aData[aIndex + 1] < bData[bIndex + 3])// 15g
			 && (aData[aIndex + 3] > bData[bIndex + 1]);// 15g
	}

	// AABB collision detection
	function collides(bytes32 aData, uint8 aIndex, bytes4 bData)
		internal pure returns (bool)
	{
		// cheap expressions before short-circuit && ops are ordered first.
		// Only pay for a couple jumpi ops (&&)
		// The huristic is if the first AND is correct more likely a collision
		
		return (aData[aIndex] < bData[2])	    // 9g
			 && (aData[aIndex + 2] > bData[0])	// 12g
			 && (aData[aIndex + 1] < bData[3])  // 12g
			 && (aData[aIndex + 3] > bData[1]); // 12g
	}

	function collidesNoBranch(bytes32 aData, uint8 aIndex, bytes4 bData)
		internal pure returns (bool)
	{
		return (aData[aIndex] < bData[2])	    // 9g
			 & (aData[aIndex + 2] > bData[0])	// 12g
			 & (aData[aIndex + 1] < bData[3])  // 12g
			 & (aData[aIndex + 3] > bData[1]); // 12g
	}

	function equals(bytes32 aData, uint8 aIndex, bytes4 bData) {
		// usually used inside a require(), so only branch within the require call
		return
			(aData[aIndex] == bData[0]) &
			(aData[aIndex + 1] == bData[1]) &
			(aData[aIndex + 2] == bData[2]) &
			(aData[aIndex + 3] == bData[3]);
	}

	function positionEquals(bytes4 aData, bytes2 bData) {
		// usually used inside a require(), so only branch within the require call
		return
			(aData[0] == bData[0]) &
			(aData[1] == bData[1]);
	}

	function copy(bytes32 sourceAndTarget, uint8 sourceIndex, uint8 targetIndex) {
		sourceAndTarget[targetIndex] = sourceAndTarget[sourceIndex];
		sourceAndTarget[targetIndex + 1] = sourceAndTarget[sourceIndex + 1];
		sourceAndTarget[targetIndex + 2] = sourceAndTarget[sourceIndex + 2];
		sourceAndTarget[targetIndex + 3] = sourceAndTarget[sourceIndex + 3];
	}

	function copy(bytes4 source, bytes32 target, uint8 targetIndex) {
		target[targetIndex] = source[0];
		target[targetIndex + 1] = source[1];
		target[targetIndex + 2] = source[2];
		target[targetIndex + 3] = source[3];
	}

	// Shifts the aabb at bytes 0 to 3 by numBytes bytes
	//function shift(bytes32 aabb, bytes32 target, uint numBytes) public pure returns (bytes32) {
	//	require(false);
		//var shifter = 256 ** numBytes
		//target & aabb[0];

		//target[index + 1] = aabb[1];
		//target[index + 2] = aabb[2];
		//target[index + 3] = aabb[3];
	//}
}