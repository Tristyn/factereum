pragma solidity ^0.4.18;
pragma experimental ABIEncoderV2;
import "./AabbLib.sol";

// A quad tree implementation with 6 layers.

// This piece of art is obsolete in favor of a Spacial Hash.
// A quadtree is only better when they handle differently sized objects.
// A quadtree is vulnerable to gas attacls
// See: http://zufallsgenerator.github.io/2014/01/26/visually-comparing-algorithms
// See: https://gamedev.stackexchange.com/a/69794

library QuadTreeLib {

	// Uses morton code http://www.forceflow.be/2013/10/07/morton
	// related stack overflow https://stackoverflow.com/q/1496009
	// related code https://stackoverflow.com/a/7828207


	// A quad is mapped by it's Tristyn code. A Tristyn code is a packed integer
	// where the lower bits are it's morton code within a layer, and higher bits
	// represent the unisgned layer number where 0 is the lowest, root layer.

	// For a quadtree with max-depth of 6 the morton code requires 6*2=12 bits,
	// and the layer number requires 3 bits to represent up to 8 layers;
	// Therefor a Tristyn code for this quadtree would be of size 16 bits.
	// Note that the highest two layers remain unused.
	
	// Consider that lower layers have larger nodes and therefor require
	// a smaller morton code. For example layer 0 has a single node and a morton
	// code of length 0, L1: 4 nodes, morton code length 2, L2, 16 nodes, morton
	// code of length 4. For Tristyn codes in lower layers, the shortened morton
	// code is stored directly to the right of the layer number, with unused
	// morton bits on the right of that simply set to 0. A Tristyn code is
	// malformed if any unused morton bits are set to 1.

	// Given the Tristyn format LLLL MMMM MMMM MMMM where L = layer, M = morton.
	// The Tristyn code for the root node would be:
	// LLLL MMMM MMMM MMMM
	// 0000 0000 0000 0000
	// To traverse into the first child node, increment the layer number and
	// set the first morton bit pair to 0x01=1:
	// LLLL MMMM MMMM MMMM
	// 0001 0100 0000 0000
	// To traverse into the fourth child node, increment the layer number and
	// set the second morton bit pair to 0x11=4:
	// LLLL MMMM MMMM MMMM
	// 0010 0111 0000 0000
	// Etc.
	
	// The mask for the morton bit pair for a layer can be created with this:
	// 0x11 << ((LAYER_MAX_VALUE - layer_num - 1) * 2)
	// Note that left shift ops are convereted to multiplication with
	// safety-checks by solc, a faster implementation would be:
	// 0x11 * 4 ** (LAYER_MAX_VALUE - layer_num - 1)
	// The morton mask of the lower layer can be determined by multiplying
	// the previous mask by 4, likewise for the lower layer and division by 4.

	// Represents a 6-deep quadtree with a byte23 usable payload per node
	struct QuadTree {
		// Note data[0:8] is used as flag storage.
		// data[0:1] to data[6:7] TristynCodes of child ids, always set.
		// data[8:11] AABB of the quad.
		// data[12] & 0x03 represent 4 flags, which children contain at least
		//   one object in its branch.
		// data[12] & 0x80 represents the initialized bit, always true when stored.
		// data[12] & 0x70 3 bits TBD
		mapping(uint16 => bytes32) quads;
	}

	//bytes32 constant rootQuad = 0x81C00180014001000 not correct
	uint16 constant tristynCodeLayerMask = 0xF000;
	uint16 constant tristynCodeMortonMask = 0x0FFF;
	// TODO: are fixed size byte arrays truely static, O(1) lookup and no SLOAD op?
	
	//uint8[16] constant populatedChildrenLeastSignificantHighBit = [100, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0];
	bytes16 constant populatedChildrenLeastSignificantHighBit = 0xFF000100020001000300010002000100;

	//uint8[4] constant populatedChildrenIndexToMaskBit = [1, 2, 4, 8];
	bytes4 constant populatedChildrenIndexToMaskBit = 0x08040201;

	uint8 constant layerMaxValue = 6;
	uint8 constant quadAabbIndex = 8;
	uint8 constant quadFlagsIndex = 12;
	bytes1 constant populatedChildrenMask = 0x03; // see populatedChildrenLeastSignificantHighBit
	uint8 constant quadInitializedBit = 0x80;


	struct TreeIterator {
		bytes32 quad;
		
		uint currentLayer;
		uint[layerMaxValue] nextChildToVisit;
		bytes4 objectAabb;
	}

	function getIterator(QuadTree storage tree) view public returns (TreeIterator memory iter) {
		uint[layerMaxValue] memory c;
		return TreeIterator({
			quad: tree.quads[0],
			currentLayer: 0,
			nextChildToVisit: c,
			objectAabb: 0x0
		});
	}

	function enumerate(QuadTree storage tree) view public {


		TreeIterator memory iter = getIterator(tree);

		do {
			//run(iter.quad);
			nextQuad(tree, iter);
		} while(iter.quad != 0);
	}

	// Iterates the quad tree for quads that intersect with the rectangle
	// returns the next quadId, or 0 if iteration complete 
	function nextQuad(
		QuadTree storage tree,
		TreeIterator memory iter
	)
		view
		public
	{
		do {

			// only visit a child if its populated and >= nextChildToVisit

			var populatedChildren = iter.quad[quadFlagsIndex] & populatedChildrenMask;

			// Given that populatedChildren is a number of binary b_0000 to b_1111
			// use a lookup table that represents the lowest bit set.
			// populatedChildren 0 (b_0000) has no children, so
			// make its lookup value terminate the loop immediately.
			var firstPopulatedChild = uint(populatedChildrenLeastSignificantHighBit[uint(populatedChildren)]);

			// firstPopulatedChild is doubled for use as the quad index, so double nextChildToVisit
			// Note firstPopulatedChild may be set abnormally high, this needs to be preserved
			var i = uint8(mathMax(iter.nextChildToVisit[iter.currentLayer], firstPopulatedChild));
			var quadI = i * 2;
			for (; i < 4; i++) {
				// short circuit quad not populated
				if (populatedChildrenIndexToMaskBit[i] & populatedChildren == 0) {
					quadI += 2;
					continue;
				}

				uint16 childId = uint16(iter.quad[quadI]) + uint16(iter.quad[quadI + 1]) * 256;
				var childQuad = tree.quads[childId];

				// short circuit quad aabb doesn't collide
				if (!AabbLib.collides(childQuad, quadAabbIndex, iter.objectAabb)) {
					quadI += 2;	
					continue;
				}

				iter.nextChildToVisit[iter.currentLayer] = i + 1;
				iter.quad = childQuad;
				// the visited byte in the higher layer may be dirty
				// clean the visited byte when we (re)enter a layer.
				iter.nextChildToVisit[++iter.currentLayer] = 0;
				return;
			}

			// Time to return down a few layers.
		} while (--iter.currentLayer >= 0);

		// Signal that iteration is complete.
		iter.quad = bytes32(0);
		// all other fields of the iterator are undefined

	}

	// probably faster to create/set all 4 child ids at once
	function createQuadChildId(uint16 parentId/*, uint16 layerNumber, uint16 childNumber*/)
		pure
		private
		returns (uint16 childId)
	{
		parentId = incrementLayerNum(parentId);
		// layerNumber is like 80000, not 1,2,3,4 etc
		//parentId = childNumber * 4 ** (layerMaxValue - layerNumber - 1);
		return parentId;
	}

	function incrementLayerNum(uint16 tristynCode) pure private returns (uint16) {
		return tristynCode + 0x1000;
	}

	function decrementLayerNum(uint16 tristynCode) pure private returns (uint16) {
		return tristynCode - 0x1000;
	}

	/**
     * Interleave bits of x and y, so the bits of x
     * are in the even positions and bits from y in the odd;
     * x and y must initially be less than 256.
	 * The only known faster implementation is a lookup table.
	 * related https://stackoverflow.com/a/7828207 
	 * with powers instead of bit shifting.
     * Adapated from http://graphics.stanford.edu/~seander/bithacks.html#InterleaveBMN
     */
	function mortonEncode(uint16 x, uint16 y) private pure returns (uint16) {
        x = (x | (x * 16)) & 0x0F0F;
        x = (x | (x * 4)) & 0x3333;
        x = (x | (x * 2)) & 0x5555;

        y = (y | (y * 16)) & 0x0F0F;
        y = (y | (y * 4)) & 0x3333;
        y = (y | (y * 2)) & 0x5555;

        return x | (y << 1);
	}

	function mathMax(uint a, uint b) private pure returns (uint) {
		// Branching (13 gas) is cheaper than bit twiddling (>15 gas?) for this operation
		// See: https://graphics.stanford.edu/~seander/bithacks.html#IntegerMinOrMax
		// See: https://docs.google.com/spreadsheets/d/1n6mRqkBz3iWcOlRem_mO09GtSKEKrAsfO7Frgx18pNU/edit
		return a > b ? a : b;
	}
}