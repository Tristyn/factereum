pragma solidity ^0.4.18;
pragma experimental ABIEncoderV2;

import "./Pausable.sol";
import "./Stuff.sol";

contract StoreBase is Pausable {

	address public Writer;

	function StoreBase() public {
		owner = msg.sender;
	}

	function setWriter(address writer) public isOwner { 
		Writer = writer;
	}

	function isStore() public pure returns (bool) {
		return true;
	}

	function seppuku(address pay) public isWriter whenNotPaused {
		selfdestruct(pay);
	}

	modifier isWriter() {
		require(Writer == msg.sender);
		_;
	}

}

contract StoreEntity is StoreBase {
	struct EntityInfo {
		bytes2 size;
		int cost;
	}

	mapping(uint => EntityInfo) entities;

	function getEntity(uint id) external view returns (EntityInfo) {
		return entities[id];
	}

	function getEntities(uint[] memory ids) external view returns (EntityInfo[] memory __) {
		return _getEntities(ids);
	}

	function _getEntities(uint[] memory ids) internal view returns (EntityInfo[] memory __) {
		EntityInfo[] memory ret = new EntityInfo[](ids.length);

		for (uint i = 0; i < ids.length; i++) {
			ret[i] = entities[ids[i]];
		}

		return ret;
	}

	function getEntitySizes(uint[] memory ids) external view returns (bytes2[] memory __) {
		return _getEntitysizes(ids);
	}

	function _getEntitySizes(uint[] memory ids) internal view returns (bytes2[] memory __) {
		bytes2[] memory ret = new bytes2[](ids.length);

		for (uint i = 0; i < ids.length; i++) {
			ret[i] = entities[ids[i]].size;
		}

		return ret;
	}

	function _getEntityCosts(uint[] memory ids) internal view returns (int[] memory __) {
		int[] memory ret = new int[](ids.length);

		for (uint i = 0; i < ids.length; i++) {
			ret[i] = entities[ids[i]].cost;
		}

		return ret;
	}

	function setEntity(EntityInfo memory entity, uint id) external isWriter {
		require(entity.cost >= 0 && id > 0);

		entities[id] = entity;
	}
}

import "./StoreInventoryLib.sol";

contract StoreInventory is StoreEntity {

	mapping(address => int[]) inventory;
	
	// Note a negative inventory value denotes a zero value.
	// Keeps the storage cell alive.
	function getInventory(address player) public view returns (int[] memory inv) {
		return inventory[player];
	}

	function getInventory(address player, uint[] memory itemIds) external view returns (int[] memory inv) {
		return _getInventory(player, itemIds);
	}

	function _getInventory(address player, uint[] memory itemIds) internal view returns (int[] memory inv) {
		int[] storage fullInventory = inventory[player];
		return StoreInventoryLib.filterInventory(fullInventory, itemIds);
	}

	function setInventory(address player, uint[] memory itemIds, int[] itemCounts) external isWriter {
		require(itemIds.length == itemCounts.length);

		int[] storage fullInventory = inventory[player];
		for(uint i = 0; i < itemIds.length; i++) {
			fullInventory[itemIds[i]] = itemCounts[i];
		}
	}
}

contract StorePartitions is StoreInventory {
	

	// The partitions indexed by their coordinates.
	// The key represents two int8 values for the X and Y coordinates.
	// Therefore there may only be 256*256 partitions.
	// packed bytes2 layout: YYYYYYYYXXXXXXXX  
	mapping(bytes2 => StorePartitionLib.Partition) _partitions;

	
	function getPartitionKey(int8 x, int8 y) external returns (bytes2) {
		return _getPartitionKey(x, y);
	}

	function _getPartitionKey(int8 x, int8 y) internal returns (bytes2) {
		return bytes2(uint8(bytes1(partitionX)) | (uint16(bytes2(partitionY)) * 2 ** 8));
	}

	function getPartition(bytes2 key) external returns (StorePartitionLib.Partition memory partition) { 
		return _getPartition(key);
	}
	
	function _getPartition(bytes2 key) internal returns (StorePartitionLib.Partition storage partition) { 
		return _partitions[key].partition;
	}

	function getPartitionByOwner(bytes2 key, uint8 ownerId) external returns (StorePartitionLib.PartitionByOwner memory partition) {
		return _getPartitionByOwner(key, ownerId);
	}

	function _getPartitionByOwner(bytes2 key, uint8 ownerId) internal returns (StorePartitionLib.PartitionByOwner memory partition) {
		StorePartitionLib.Partition storage p = _getPartition(key);
		partition.owner = p.owners[ownerId];

		partition.machinesLength = p.machinesLength;
		partition.machines = p.machines[ownerId];
		partition.beltsLength = p.beltsLength;
		partition.belts = p.belts[ownerId];
	}

	function setPartitionByOwner(bytes2 key, uint8 ownerId, StorePartitionLib.PartitionByOwner memory partition) external isWriter {
		StorePartitionLib.PartitionFull storage p = _getPartition(key);
		
		var currentOwner = p.owners[ownerId];
		if (currentOwner != partition.owner) {
			p.owners[ownerId] = partition.owner;
		}

		p.machinesLength[ownerId] = partition.machinesLength;
		p.machines[ownerId] = partition.machines;
		p.beltsLength[ownerId] = partition.beltsLength;
		p.belts[ownerId] = partition.belts;
	}
}

contract StoreSpatialHash is StorePartitions {
	using SpatialHashLib for StorePartitionLib.Partition;



	function getSpatialHash(uint x, uint y) external returns (bytes2) {
		return SpatialHashLib.getSpatialHash(x ,y);
	}

	function _getSpatialHash(uint x, uint y) internal returns (bytes2) {
		return SpatialHashLib.getSpatialHash(x, y);
	}
 
	function getSpatialBucket(bytes2 partitionKey, bytes2 spatialHash)
		external view returns (bytes32)
	{
		StorePartitionLib.Partition storage partition = _getPartition(partitionKey);
		partition.getSpatialBucket(spatialHash);
	}

	function getSpatialBuckets(bytes2 partitionKey, SpatialHashLib.SpatialBucketsQuery memory query)
		external view returns (bytes32[][] memory buckets)
	{
		return _getSpatialBuckets(partitionKey, query);
	}

	function _getSpatialBuckets(bytes2 partitionKey, SpatialHashLib.SpatialBucketsQuery memory query)
		internal view returns (bytes32[][] memory buckets)
	{
		StorePartitionLib.Partition storage partition = _getPartition(partitionKey);
		return partition.getSpatialBuckets(spatialHashKeys, query);
	}

	function getSpatialBucketsForOwner(bytes2 partitionKey, SpatialHashLib.SpatialBucketsQuery memory query, uint8 ownerId)
		external view returns (bytes32[][] memory buckets)
	{
		StorePartitionLib.Partition storage partition = _getPartition(partitionKey);

		require(partition.owners[ownerId] == player);

		return partition.getSpatialBucketsForOwner(spatialHashKeys, query, ownerid);
	}

	function setSpatialBuckets(bytes32[][] memory buckets, SpatialHashLib.SpatialBucketsQuery memory query) isWriter
		external
	{
		StorePartitionLib.Partition storage partition = _getPartition(partitionKey);

		partition.setSpatialBuckets(partition, query, buckets);
	}
}

contract Store is StoreSpatialHash {
	
}