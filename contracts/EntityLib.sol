pragma solidity ^0.4.18;
pragma experimental ABIEncoderV2;

library EntityLib {
	uint internal constant nullId = 0;
	uint internal constant barrierId = 1;
	uint internal constant coinId = 2;
	uint internal constant beltId = 3;

	// all special entities will be <= specialIdsMask
	uint internal constant specialIdsMask = 3;
	uint internal constant minimumItemIdForMachines = 4;

	uint internal constant maxSize = 9;

	// Description of an in-memory entity that will get removed
	struct MachineRemoval {
		// index into ChangeOp.associatedItemIds
		uint itemIndex;
		// Note this only denotes the position.
		// For barriers last 2 bytes are used as an AABB
		bytes4 position;

		// The uint2 indexes of the entity within each bucket that it collides with.
		bytes32 bucketPositions;

		// The index into machines list
		uint machinesIndex;
	}

	// Description of an in-memory entity that will get removed
	struct BeltRemoval {
		// index into ChangeOp.associatedItemIds
		uint itemIndex;
		// Note this only denotes the position.
		// For barriers last 2 bytes are used as an AABB
		bytes4 position;

		// The uint2 index of the entity within its hash bucket.
		uint2 bucketPosition;

		// The index into machines list
		uint beltsIndex;
	}

	struct MachineAddition {
		uint itemIndex;

		// Note this only denotes the position.
		// For barriers last 2 bytes are used as an AABB
		bytes4 position;
	}

	struct BeltAddition {
		// index into ChangeOp.associatedItemIds
		uint itemIndex;
		
		// Note this only denotes the position.
		// For barriers last 2 bytes are used as an AABB
		bytes4 position;

		bytes2 connections;
	}
	
	function isBelt(uint itemId) internal pure returns(bool) {
		return itemId == EntityLib.beltId
	}

	function isMachine(uint itemId) internal pure returns(bool) {
		return itemId >= EntityLib.minimumItemIdForMachines;
	}
}