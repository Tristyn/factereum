pragma solidity ^0.4.18;
pragma experimental ABIEncoderV2;

import "./AabbLib.sol";
import "./SpatialBucketLib.sol";

contract SpatialHashLib is AabbLib {
	uint8 constant cellSize = 2;
	uint8 public constant cellSizeAlignmentMask = 0xFE;
	uint8 constant bucketAabbSize = 4;
	uint8 constant bucketLengthMax = bucketAabbSize * 4;
	uint8 constant bucketLengthIndex = 31;
	uint8 constant bucketActiveBitIndex = 30;
	bytes32 constant bucketActiveBitMask = 0x0001000000000000000000000000000000000000000000000000000000000000;

	struct SpatialBucketsQuery {
		uint8 yStart;
		uint8 xStart;
		// list of uint8 lengths for each y row
		// starting at 0
		bytes128 xLengths;

		// Each array entry is a y row starting at yStart
		// Each bytes16 is a list of bools starting at xStart where true
		// will fetch the bucket and false leaves it zeroed.
		bytes16[] include;
	}

	function getSpatialBucket(StorePartitionLib.Partition storage map, bytes2 spatialHash)
		internal view returns (bytes32)
	{
		return map.spatialBuckets[spatialHash];
	}

	function getSpatialBuckets(StorePartitionLib.Partition storage map, SpatialBucketsQuery memory query)
		internal view returns (bytes32[][] memory ret)
	{
		uint yEnd = uint(query.yStart) + query.xLengths.length;
		require(
			(yEnd < 128)) &
			(query.include.length >= yEnd)
		)

		ret = new bytes32[][](query.xLengths.length);


		// precalculate these `helper` variables to speed up the algorithm
		uint16 yPacked = query.yStart * 2 ** 9; 
		uint16 incrementOfYPacked = 2 ** 9;

		for (uint y = query.yStart; y < yEnd; y++) {
			Bytes32[] memory bucketRow = new bytes32[](query.xLengths[y]);
			var xEnd = uint(query.xLengths[y]) + query.xStart;
			bytes16 includes = query.include[y - query.yStart];
			bytes16 lowestBitMask = 0x01
			require(xEnd < 128);

			for (uint j = query.xStart; j < xEnd; j++) {
				var shouldSkip = includes & lowestBitMask == 0
				if (shouldSkip) { 
					continue;
				}

				var bucket = map.spatialBuckets[bytes2(yPacked | (j * 2))];
				bucketRow[j - query.xStart];

				// shift right 1 bit
				shouldSkip /= 2;
			}

			ret[y - query.yStart] = bucketRow;
			yPacked += incrementOfYPacked;
		}

		return ret;
	}

	// Gets the spacial buckets and checks that the owner id matches
	function getSpatialBucketsForOwner(StorePartitionLib.Partition storage map, SpatialBucketsQuery memory query, uint8 ownerId)
		internal view returns (bytes32[][] memory ret)
	{
		uint yEnd = uint(query.yStart) + query.xLengths.length;
		//require(
		bool requireConditions = 
			(yEnd < 128)) &
			(query.include.length >= yEnd);
		//)

		ret = new bytes32[][](query.xLengths.length);


		// precalculate these `helper` variables to speed up the algorithm
		uint16 yPacked = query.yStart * 2 ** 9; 
		uint16 incrementOfYPacked = 2 ** 9;

		for (uint y = query.yStart; y < yEnd; y++) {
			Bytes32[] memory bucketRow = new bytes32[](query.xLengths[y]);
			var xEnd = uint(query.xLengths[y]) + query.xStart;
			bytes16 includes = query.include[y - query.yStart];
			bytes16 lowestBitMask = 0x01
			require(xEnd < 128);

			for (uint j = query.xStart; j < xEnd; j++) {
				var shouldSkip = (includes & lowestBitMask) == 0;
				if (shouldSkip) { 
					continue;
				}

				var bucket = map.spatialBuckets[bytes2(yPacked | (j * 2))];

				// check owner id
				requireConditions = requireConditions & bucket[SpatialBucketLib.bucketOwnerIdIndex] == ownerId

				bucketRow[j - query.xStart] = bucket;

				// shift right 1 bit
				includes /= 2;
			}

			ret[y - query.yStart] = bucketRow;
			yPacked += incrementOfYPacked;
		}

		require(requireConditions);

		return ret;
	}

	
	// Gets the spacial buckets and checks that the owner id matches
	function setSpatialBucketsForOwner(StorePartitionLib.Partition storage map, SpatialBucketsQuery memory query, bytes32[][] memory buckets)
		internal
	{
		uint yEnd = uint(query.yStart) + query.xLengths.length;
		require(
		//bool requireConditions = 
			(yEnd < 128)) &
			(query.include.length >= yEnd)
		)

		ret = new bytes32[][](query.xLengths.length);


		// precalculate these `helper` variables to speed up the algorithm
		uint16 yPacked = query.yStart * 2 ** 9; 
		uint16 incrementOfYPacked = 2 ** 9;

		for (uint y = query.yStart; y < yEnd; y++) {
			var xEnd = uint(query.xLengths[y]) + query.xStart;
			require(xEnd < 128);

			var bucketsRow = buckets[y - query.yStart];
			for (uint j = query.xStart; j < xEnd; j++) {
				var bucket = bucketsRow[j - query.xStart];

				var shouldSkip = bucket[bucketActiveBitIndex] == 0;
				if (shouldSkip) { 
					continue;
				}
				
				var bucketKey = bytes2(yPacked | (j * 2));
				var storedBucket = map.spatialBuckets[bucketKey];
				if (storedBucket != bucket) {
					map.spatialBuckets[bucketKey] = bucket;
				}
			}

			yPacked += incrementOfYPacked;
		}
		
	}

	//function collides(SpatialHash storage map, bytes4 aabb) public pure returns (bool) {
	//	return true;
	//}

	function getSpatialHash(uint8 x, uint8 y) internal pure returns (bytes2) {
		x = x & cellSizeAlignmentMask;
		y = y & cellSizeAlignmentMask;

		return getSpatialHashUnsafe(x, y);
	}

	function getSpatialHashUnsafe(uint8 x, uint8 y) internal pure returns(bytes2) {
		return bytes2(x | uint16(y * 2 ** 8));
	}

	function getRange(bytes4 rect)
		private pure
		returns (uint8 x1, uint8 y1, uint8 x2, uint8 y2)
	{
		x1 = uint8(rect[0]) / cellSize;
		y1 = uint8(rect[1]) / cellSize;
		x2 = uint8(rect[2]) / cellSize;
		y2 = uint8(rect[3]) / cellSize;
	}

	function activateBucket(bytes32 bucket) private pure returns (bytes32) {
		return bucket | bucketActiveBitMask;
	}
	
	function mathMax(uint8 a, uint8 b) private pure returns (uint) {
		// Branching (13 gas) is cheaper than bit twiddling (>15 gas?) for this operation
		// See: https://graphics.stanford.edu/~seander/bithacks.html#IntegerMinOrMax
		// See: https://docs.google.com/spreadsheets/d/1n6mRqkBz3iWcOlRem_mO09GtSKEKrAsfO7Frgx18pNU/edit
		return a > b ? a : b;
	}

	function mathMin(uint8 a, uint8 b) private pure returns (uint) {
		// Branching (13 gas) is cheaper than bit twiddling (>15 gas?) for this operation
		// See: https://graphics.stanford.edu/~seander/bithacks.html#IntegerMinOrMax
		// See: https://docs.google.com/spreadsheets/d/1n6mRqkBz3iWcOlRem_mO09GtSKEKrAsfO7Frgx18pNU/edit
		return a > b ? a : b;
	}
}