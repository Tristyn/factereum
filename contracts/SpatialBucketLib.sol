pragma solidity ^0.4.18;
pragma experimental ABIEncoderV2;

import "./EntityLib.sol";

library SpatialBucketLib {
	// Methods for reading and modifying spatial buckets.

	// A 2x2 bucket mapped to the position of it's min vertice.
	// A bucket must be entirely controlled by one player, to prevent two transactions
	// invalidating a bucket modification.
	// The map key is two uint8's packed to a bytes2, each component is a multiple of cellSize.
	// data[0:3] to data[12:15] object aabb, length 4
	// data[21] owner id. Maps to StorePartitionLib.Partition.owners
	// data[22:25] to data[26:29] & 0x0F isFloor bits Maybe?
	// data[31] & 0x03 length
	// data[30] & 0x01 active bit: > 0 once stored

	uint8 constant bucketAabbSize = 4;
	uint8 constant bucketAabbLengthMax = 4;
	uint8 constant bucketAabbIndexMax = bucketAabbSize * bucketAabbLengthMax;
	uint8 constant bucketLengthIndex = 31;
	uint8 constant bucketActiveBitIndex = 30;
	uint8 constant bucketActiveBitMask = 0x01;
	uint8 internal constant bucketOwnerIdIndex = 21;
	uint8 constant bucketFloorAabbIndex = 22;
	uint8 constant bucketFloorAabbLengthMax = 4;
	uint8 constant bucketFloorAabbIndexMax = bucketAabbSize * bucketFloorAabbLengthMax;
	uint8 constant buckerFloorMask = 0x0F;


	// Inserts an entities aabb into the bucket, increments length.
	// Checks for machine and floor aabb collisions in the buckets, that the active
	// bit is set, and that the buckets are not full.
	// The idea is that the store will only return buckets that are owned by the player
	// and that any zero buckets (no active bit) may be for another player.
	function addAabb(byts32[][] memory bucketsYX, uint8 startX, uint8 startY, bytes4 entityPosition)
		internal
	{
		// iterate by Y, X because filteredBuckets is grouped by Y, X
		for (var j = entityPosition[1] / 2; j < (entityPosition[3] + 1) / 2; j++) {
			var buckets = bucketsYX[j -  startY];
			for (var i = entityPosition[0] / 2; i < (entityPosition[2] + 1) / 2; i++) {
				
				var bucket = bucket[i - startX];
				var bucketLength = bucket[bucketLengthIndex];
				var emptyBucketAabbIndex = bucketLength * bucketAabbSize;
				
				AabbLib.copy(entityPosition, bucket, emptyBucketAabbIndex);
				
				// Check for aabb collisions
				bool collides = false; 
				for (var k = 0; k < emptyBucketAabbIndex; k += bucketAabbSize) {
					collides = collides | AabbLib.collidesNoBranch(bucket, k, entityPosition);
				}


				// Check that we collide with one floor aabb
				// That's enough to know we aren't partially out of bounds
				// because each building will be surounded by barriers
				// and you can't both collide with a floor but also not collide
				// with a barrier and be out of bounds.
				if (AabbLib.collidesNoBranch(bucket, bucketFloorAabbIndex, entityPosition) == false) {
					collides = collides | !AabbLib.collidesNoBranch(bucket, bucketFloorAabbIndex + bucketAabbSize, position);
				}


				var newLength = bucketLength + 1;
				require(
					(newLength < bucketAabbLengthMax) & 
					(collides == false) &
					(bucket[bucketActiveBitIndex] > 0)
				);

				bucket[bucketLengthIndex] = newLength;
				buckets[i - startX] = bucket;
			}
		}
	}

	// Removes an entity from the buckets by overwriting its aabb with the
	// last entitys aabb and decrementing bucket length.
	// Any aabbs >= length are undefined.
	// Only asserts that all the buckets needed are provided, the aabb matches and
	// does a length check within a bucket.
	// Does not check the itemId, ownership or anything else.
	// Doesn't check the active bit because the store will only return buckets
	// that are owned by the player and that any zero buckets (no active bit) will
	// fail either the main aabb test or floor aabb test.
	function removeAabb(bytes32[][] memory bucketsYX, uint8 startX, uint8 startY, bytes4 aabb, bytes32 bucketPositions) 
		internal
	{

		// iterate by Y, X because filteredBuckets is grouped by Y, X
		for (var j = aabb[1] / 2; j < (aabb[3] + 1) / 2; j++) {
			var buckets = bucketsYX[j -  startY];
			for (var i = aabb[0] / 2; i < (aabb[2] + 1) / 2; i++) {
				
				var bucket = buckets[i - startX];
				var bucketLength = bucket[bucketLengthIndex];
				
				// If a type is explicitly converted to
				// a smaller type, higher-order bits are cut off.. Perfect!
				var bucketPosition = uint2(bucketPositions);
				var bucketIndex = bucketPosition * bucketAabbSize;	

				require(
					(bucketLength > 0) &
					(bucketPosition < bucketLength) &
					(AabbLib.equals(bucket, bucketIndex, aabb))
				);

				// remove the entity by overwriting its aabb with the
				// last entitys aabb and decrementing bucket length.
				AabbLib.copy(bucket, ((bucketLength - 1) * bucketAabbSize), bucketIndex);
				bucket[bucketLengthIndex] = bucketLength - 1;

				buckets[i - startX] = bucket;
				bucketPositions /= 2 ** 2;
			}
		}
	}
}
