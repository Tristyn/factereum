pragma solidity ^0.4.18;
pragma experimental ABIEncoderV2;

library StoreInventoryLib {
	function filterInventory(int[] storage inventory, uint[] memory itemIds) internal view returns (int[] memory inv) {
		int[] memory ret = new int[](itemIds.length);

		for (uint i = 0; i < itemIds.length; i++) {
			ret[i] = inventory[itemIds[i]];
		}

		return ret;
	}

	// Not sure how to define a memory array of int storage arrays
	function filterInventories(int[][] storage inventories, uint[][] memory itemIds) internal pure returns(int[][] memory returned){ 
		require(inventories.length == itemIds.length);

		/* From docs on multiDimensional arrays
		 * http://solidity.readthedocs.io/en/develop/types.html#arrays
		 * 
		 * As an example, an array of 5 dynamic arrays of uint is uint[][5]
		 * (note that the notation is reversed when compared to some other languages).
		 * To access the second uint in the third dynamic array, you use x[2][1]
		 * (indices are zero-based and access works in the opposite way of the
		 * declaration, i.e. x[2] shaves off one level in the type from the right)
		 */
		

		int[][] memory ret = new int[][](itemIds.length);

		for (uint i = 0; i < itemIds.length; i++) {
			int[] inventory = inventories[i];
			uint[] _itemIds = itemIds[i];

			int[] filtered = new int[](_itemIds.length);

			for (uint j = 0; j < _itemIds.length; j++) {
				filtered[j] = inventory[_itemIds[j]]
			}

			ret[i] = filtered;
		}

		return ret;
	}
}