pragma solidity ^0.4.18;
pragma experimental ABIEncoderV2;

library StorePartitionLib {

	struct PartitionFull {
		// A Spatial Hash for a 256x256 region
		// A 2x2 bucket mapped to the position of it's min vertice.
		// The map key is two uint8's packed to a bytes2, each component is a multiple of cellSize.
		mapping(bytes2 => bytes32) spatialBuckets;
		Partition partition;
	}

	struct Partition {
		address[] owners;

		// for these fields, first index is the owner id
		uint[] machinesLength;
		PartitionMachine[][] machines;
		uint[] beltsLength;
		PartitionBelt[][] belts;
	}

	struct PartitionByOwner {
		address owner;

		uint machinesLength;
		PartitionMachine[] machines;
		uint beltsLength;
		PartitionBelt[] belts;
	}

	struct PartitionMachine {
		uint itemId;
		bytes4 position;
	}

	struct PartitionBelt {
		bytes4 position;
		bytes2 connections;
	}
}