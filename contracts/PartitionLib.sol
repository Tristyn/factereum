pragma solidity ^0.4.18;
pragma experimental ABIEncoderV2;

import "./Store.sol";
import "./StorePartitionLib.sol";
import "./EntityLib.sol";
import "./InventoryLib.sol";
import "./SpatialHashLib.sol";
import "./SpatialBucketLib.sol";

library PartitionLib {
	using InventoryLib for InventoryLib.MemoryInventory;

	struct ChangeOp {
		RemoveOp remove;
		address player;
		uint8 playerOwnerId;
		bytes2 partitionKey;


		EntityLib.MachineRemoval[] toRemoveMachines;
		EntityLib.BeltRemoval[] toRemoveBelts;
		EntityLib.MachineAddition[] toAddMachines;
		EntityLib.BeltAddition[] toAddBelts;


		// Fetched
		InventoryLib.MemoryInventory inventory;
		// grouped by Y, then X!
		bytes32[][] filteredBuckets;
		StorePartitionLib.PartitionByOwner partition;

		// Fields precomputed offchain for speed
		// item ids ascending.
		uint[] associatedItemIds;
		// Spacial hash buckets used in collisions
		SpatialHashLib.SpatialBucketsQuery associatedSpatialBuckets;
	}



	function changePartition(ChangeOp memory changes, Store store) internal {
		fetchFromStore(changes, store);
		changeEntities(changes);
		saveToStore(changes, store);
	}

	function fetchFromStore(ChangeOp memory changes, Store store) internal {
		changes.inventory = MemoryInventory({
			shortInventory: store.getInventory(changes.player, changes.associatedItemIds),
			changes.filteredEntities: store.getEntities(changes.associatedItemIds),
			itemIdsMap: changes.associatedItemIds
		});
		changes.filteredBuckets = store.getSpatialBucketsForOwner(
			changes.partitionKey,
			changes.associatedSpatialBuckets,
			changes.playerOwnerId
		);
		changes.partition = store.getPartitionByOwner(changes.partitionKey, changes.playerOwnerId);
		
		require(changes.partition.owners[changes.playerOwnerId] == changes.player);
	}

	function saveToStore(ChangeOp memory changes, Store store) {
		store.setInventory(changes.player, changes.associatedItemIds, changes.inventory.shortInventory);
		store.setSpatialBuckets(changes.filteredBuckets, changes.associatedSpatialBuckets);
		store.setPartitionByOwner(changes.partitionKey, changes.partition);
	}


	function changeEntities(ChangeOp memory changes) internal {
		removeEntities(changes);
		ensureEntityListLengths(changes);
		addEntities(changes);
	}


	function removeEntities(ChangeOp memory changes) internal {
		var startX = changes.associatedSpatialBuckets.startX;
		var startY = changes.associatedSpatialBuckets.startY;

		// remove machines
		for (var i = 0; i < changes.toRemoveMachines.length; i++) {
			var entity = changes.toRemoveMachines[i];

			// We do not trust entity size, only position
			var size = changes.inventory.getEntity(entity.itemIndex).size;
			entity.position[2] = entity.position[0] + size[0];
			entity.position[3] = entity.position[1] + size[1];

			
			SpatialBucketLib.removeAabb(
				changes.filteredBuckets, startX, startY, entity.position, entity.bucketPositions);
			removeEntityFromPartitionList(changes, entity);
			changes.inventory.incrementItemCount(entity.itemIndex);
			removeMachineFromPartitionList(changes, entity);
			
			require(EntityLib.isMachine(entity.itemId));
		}


		// remove belts
		for (var i = 0; i < changes.toRemoveBelts.length; i++) {
			var entity = changes.toRemoveBelts[i];

			// We do not trust entity size, belts are always 1 wide
			entity.position[2] = entity.position[0] + 1;
			entity.position[3] = entity.position[1] + 1;



			SpatialBucketLib.removeAabb(
				changes.filteredBuckets, startX, startY, entity.position, bytes32(entity.bucketPosition));
			
			var itemId = changes.inventory.getItemId(entity.itemIndex);
			changes.inventory.incrementItemCount(entity.itemIndex);
			removeBeltFromPartitionList(changes, entity);
			
			require(EntityLib.isBelt(itemId));
		}
	}

	function ensureEntityListLengths(ChangeOp memory changes) internal {
		var partition = changes.partition;

		var emptyMachineSlots = partition.machines.length - partition.machinesLength;
		if (emptyMachineSlots < changes.toAddMachines.length) {
			PartitionMachine[] memory newMachines = new PartitionMachine[](
				partition.machinesLength + changes.toAddMachines.length);
			for(uint i = 0; i < partition.machinesLength; i++) {
				newMachines[i] = partition.machines[i];
			}
			partition.machines = newMachines;
		}

		var emptyBeltSlots = partition.belts.length - partition.beltsLength;
		if (emptyBeltSlots < changes.toAddBelts.length) {
			PartitionBelt[] memory newBelts = new PartitionBelt[](
				partition.beltsLength + changes.toAddBelts.length);
			for(uint i = 0; i < partition.beltsLength; i++) {
				newBelts[i] = partition.belts[i];
			}
			partition.belts = newBelts;
		}
	}

	function removeBeltFromPartitionList(ChangeOp memory changes, EntityLib.BeltRemoval memory entity)
		internal
	{
		var entry = changes.partition.belts[entity.beltsIndex];
		require(
			(AabbLib.positionEquals(entity.position, entry.position)) & 
			(entity.beltsIndex < changes.partition.beltsLength)
		);

		changes.partition.beltsLength--;
		changes.partition.belts[entity.beltsIndex] =
			changes.partition.belts[changes.partition.beltsLength];
	}

	function removeMachineFromPartitionList(ChangeOp memory changes, EntityLib.MachineRemoval memory entity)
		internal
	{
		var entry = changes.partition.machines[entity.machinesIndex];
		require(
			(AabbLib.positionEquals(entity.position, entry.position)) &
			(entity.machinesIndex < changes.partition.machinesLength)
		);

		changes.partition.machinesLength--;
		changes.partition.machines[entity.machinesIndex] =
			changes.partition.machines[changes.partition.machinesLength];
	}

	function addEntities(ChangeOp memory changes) internal {
		var startX = changes.associatedSpatialBuckets.startX;
		var startY = changes.associatedSpatialBuckets.startY;


		// add machines
		for (var i = 0; i < changes.toAddMachines.length; i++) {
			var entity = changes.toAddMachines[i];

			// We do not trust entity size, only position
			var size = changes.filteredEntities[entity.itemIndex].size;
			entity.position[2] = entity.position[0] + size[0];
			entity.position[3] = entity.position[1] + size[1];
			
			SpatialBucketLib.addAabb(
				changes.filteredBuckets, startX, startY, entity.position);
			changes.inventory.decrementItemCount(entity.itemIndex);
			addMachineToPartitionList(changes, entity);
		}

		// add belts
		for (var i = 0; i < changes.toAddBelts.length; i++) {
			var entity = changes.toAddBelts[i];

			// We do not trust entity size, only position
			var size = changes.filteredEntities[entity.itemIndex].size;
			entity.position[2] = entity.position[0] + 1;
			entity.position[3] = entity.position[1] + 1;
			
			SpatialBucketLib.addAabb(
				changes.filteredBuckets, startX, startY, entity.position);
			changes.inventory.decrementItemCount(entity.itemIndex);
			addBeltToPartitionList(changes, entity);
		}
	}

	function addMachineToPartitionList(ChangeOp memory changes, EntityLib.MachineAddition memory entity) 
		internal
	{
		var itemId = changes.inventory.getItemId(entity.itemIndex);
		require(
			EntityLib.isMachine(itemId)
		);
		changes.partition.machines[changes.partition.machinesLength] = PartitionMachine({
			itemId: itemId,
			position: entity.position
		});
		changes.partition.machinesLength++;
	}

	function addBelteToPartitionList(ChangeOp memory changes, EntityLib.BeltAddition memory entity) 
		internal
	{
		var itemId = changes.inventory.getItemId(entity.itemIndex);
		require(
			EntityLib.isBelt(itemId)
		);
		changes.partition.belts[changes.partition.beltsLength] = PartitionBelt({
			position: position,
			connections: entity.connections
		});

		changes.partition.beltsLength++;
	}

}