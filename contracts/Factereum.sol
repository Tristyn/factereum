pragma solidity ^0.4.18;
pragma experimental ABIEncoderV2;

import "./DevAccessControl.sol";
import "./Store.sol";
import "./Stuff.sol";

contract FactereumBase is DevAccessControl { 
	Store _store;

	/// @dev Sets the reference to the siring auction.
    /// @param _address - Address of siring contract.
    function setStoreAddress(address _address) external onlyCEO {
        Store store = Store(_address);

        // NOTE: verify that a contract is what we expect
        require(store.isStore());

        // Set the new contract address
        _store = store;
    }
}

contract FactereumUpgradable is FactereumBase {
	address _newContractAddress;

    event ContractUpgrade(address newContract);

    /// @dev Used to mark the smart contract as upgraded, in case there is a serious
    ///  breaking bug. This method does nothing but keep track of the new contract and
    ///  emit a message indicating that the new address is set. It's up to clients of this
    ///  contract to update to the new contract address in that case. (This contract will
    ///  be paused indefinitely if such an upgrade takes place.)
    /// @param _v2Address new address
    function setNewAddress(address _v2Address) external onlyCEO whenPaused {
        _newContractAddress = _v2Address;
        ContractUpgrade(_v2Address);
    }

    /// @dev Override unpause so it requires all external contract addresses
    ///  to be set before contract can be unpaused. Also, we can't have
    ///  newContractAddress set either, because then the contract was upgraded.
    /// @notice This is public rather than external so we can call super.unpause
    ///  without using an expensive CALL.
    function unpause() public onlyCEO whenPaused {
        require(_store != address(0));
        require(_newContractAddress == address(0));

        // Actually unpause the contract.
        super.unpause();
    }
}

contract FactereumPartitions is FactereumUpgradable {
        
}

contract Factereum is FactereumPartitions {

	function Factereum() public {
		_paused = true;
	 
	    // the creator of the contract is the initial CEO
        ceoAddress = msg.sender;

        // the creator of the contract is also the initial COO
        cooAddress = msg.sender;
	}

	function seppuku(address pay) onlyCEO public  {
		selfdestruct(pay);
	}
}