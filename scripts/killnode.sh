# Many scripts here leak node processes, though it is intentional in runnodes.sh.
# This script kills all node instances for the user, freeing ports and resources.

# if msys2 terminal (git bash) or win cmd
if [ "$OSTYPE" == "msys" ] || [ "$OSTYPE" == "win32" ] ; then
	taskkill //F //IM node.exe
else
	# all other platforms, assume linux
	killall node
fi