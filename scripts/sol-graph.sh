#!/bin/bash

#solgraph cant handle certain tokens such as pragma experimental $foo;, remove them
solgraph_source_lines_to_strip_regexpression=$"pragma experimental"
solg_strip=$solgraph_source_lines_to_strip_regexpression

mkdir -p bin/solgraph
for file in contracts/*.sol
do
	# ${filename%.*} strips the last extension, from https://stackoverflow.com/a/40621571
	filename=$(basename ${file%.*})
	cat $file | grep -v "$solg_strip" | solgraph | dot -Tpng > bin/solgraph/$filename.png;
done