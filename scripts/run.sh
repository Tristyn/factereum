./scripts/killnode.sh

./scripts/runchain.sh &

./scripts/runserver.sh &

# after this ran for a bit ./scripts/killnode.sh ended ~50 node processes
#npm run grunt watch &

npm run webpack -- -w &

(truffle compile; truffle watch) &

wait < <(jobs -p)