#!/bin/bash

./scripts/runchain.sh >/dev/null &

# Gotta run embark synchronous because grunt relies on it.  
#cd src/contract_server
#embark build > /dev/null # Fucking annoying solidity warnings.
#cd - > /dev/null

#(truffle comile; solstice ./bin/contracts ./bin/contracts)
truffle compile

#./scripts/sol-graph.sh &

# dont run webpack inside grunt. Grunt triggers it twice.
npm run grunt
npm run webpack

wait < <(jobs -p)