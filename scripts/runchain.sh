#!/bin/bash

echo "Running dev chain in the background if one doesn't exist. (on Node.js)"
# Note killing the command will not close the node process so no point sleeping until the command gets cancelled.

#old non-embark way
#geth --testnet --nodiscover --maxpeers 0 --rpc --rpcport "8080" --rpccorsdomain "localhost" --networkid 167618481 --identity "TristynMainNode" --verbosity 2 --datadir .chains/test console

#embark
#cd src/contract_server
#embark simulator
#cd - > /dev/null

mkdir -p "./bin/chains/dev"
npm run ganache-cli -- --host localhost --deterministic --accounts 10 --networkId 3069873 --db "bin\\chains\\dev" --mnemonic "fuck fuck fuck fuck fuck fuck fuck" --gasPrice 5000000000 --gasLimit 7980530 

# gas settings estimated from the eth mainnet
# mnemonic shouldn't normally be required, see https://github.com/trufflesuite/ganache-cli/issues/407

#wait < <(jobs -p)